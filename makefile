JFLAGS = -g
JC = javac
JVM= java
CLASSPATH=./src
GENDIR=./classes
MAIN = ./classes
JARPATH=./d_jar/mysql-connector.jar
JAVA=$(shell find $(SRC) -type f -name '*.java') 
JARDEST=target
CLASSDEST=classes
target=temp.jar
.SUFFIXES: .java .class
.java.class: 
	$(JC) -classpath  $(CLASSPATH) -d $(GENDIR) $(JFLAGS) $*.java
	

CLASSES =\
	./src/xmlReader.java	\
	./src/getRecord.java

MAIN = ./classes

default: classes

classes:\
	$(CLASSES:.java=.class)


clean:
	$(RM) *.class
