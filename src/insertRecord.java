
import java.net.URL;
import java.sql.*;
import java.io.*;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

public class getRecord {
 public static void main(String[] args) {
  try {
    Connection con = null;
  
  String xmlFile = "./xml/config.xml";
   
    
    
    System.out.println("Path: "+xmlFile);
    xmlReader xReader = new xmlReader();
    String strChangeNo;
    String strVerTag;
    String strHostname;
    String strPDB;
    String strKuber;
    String strDIR;
    String strOSUser;
    
    boolean blDebug = false;
    if (args.length < 7) {
    	 System.out.println("-- getRecord");
         System.out.println("version 1.0 Java");
         System.out.println("getRecord expects 1 argument");
        
         throw new Exception("Program call missing parameter");
      
    } else {
     
    }
    strChangeNo = args[0];
    strVerTag = args[1];
    strHostname = args[2];
    strPDB = args[3];
    strKuber = args[4];
    strDir = args[5];
    strOSUser = args[6]
      
		java.util.Date date=new java.util.Date();
		java.sql.Date sqlDate=new java.sql.Date(date.getTime());
		java.sql.Timestamp sqlTime=new java.sql.Timestamp(date.getTime());

    if (args.length > 1) {
      blDebug = true;
      System.out.println("Debugging output is ON");
    }
    //Class.forName("net.sourceforge.jtds.Driver");
    //For mysql
    Class.forName("com.mysql.jdbc.Driver");
    String strConnUser = xReader.getElementValue(xmlFile, "ConnectionStringUser");
    String strConnPass = xReader.getElementValue(xmlFile, "ConnectionStringPassword");
    if (strConnUser == null) {
     //strConnUser = "sa";
    	strConnUser = "root";
    }
    if (strConnPass == null) {
     //strConnPass = "initial123";
    	 strConnPass = "root";
    }

    //String strConnection = "jdbc:jtds:sqlsedrver://" + xReader.getElementValue(xmlFile, "IPAddress") + ":1433/ejs;user=" + strConnUser + ";password=" + strConnPass;
    String strConnection = "jdbc:mysql://" + xReader.getElementValue(xmlFile, "IPAddress") + ":3306/ejs";
    if (blDebug) {
      System.out.println("IPAddress from config file: " + xReader.getElementValue(xmlFile, "IPAddress"));
    }
   // con = DriverManager.getConnection(strConnection);
    //for mysql
    con = DriverManager.getConnection(strConnection,strConnUser,strConnPass);
    
    String strQuery = "INSERT INTO deploys VALUES ("+strChangeNo+", "+strVerTag+", "+strHostname+", "+strPDB+", "+strKuber+", "+strDIR+", "+strOSUser+", "+sqlDate+", "+sqlTime+")";
    if (blDebug) {
      System.out.println("Query; " + strQuery);
    }
    Statement stmt = con.createStatement();
    ResultSet srs = stmt.executeQuery(strQuery);
  } 
    con.close();
  }catch (Exception e) {
	  e.printStackTrace(); 
   System.out.println("Exception: " + e.getMessage());
  }
 }
}
